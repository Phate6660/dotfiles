Note: Everything `mpv` related has been moved to [mpv-config](https://codeberg.org/Phate6660/mpv-config).<br>
I got very tired of Github telling me that over half my dotfiles were written in Lua.

`$ tree -a -I .git`

```
.
├── .alpine -- alpine-specific aliases, environmental variables, and/or functions to be sourced in .bashrc
├── ascii -- ascii art for use in rsfetch, neofetch, cat, or anything really
│   ├── ahegao
│   ├── anime
│   ├── girl
│   ├── girl2
│   ├── girl3
│   ├── girl4
│   ├── girl5
│   ├── goku
│   ├── neko
│   ├── neko2
│   ├── neko3
│   ├── tifa
│   ├── totoro
│   └── vash
├── .bashrc -- bash config
├── .cache
│   └── wal -- color schemes for various things
│       ├── colors
│       ├── colors.css
│       ├── colors.hs
│       ├── colors.json
│       ├── colors-kitty.conf
│       ├── colors-konsole.colorscheme
│       ├── colors-oomox
│       ├── colors-putty.reg
│       ├── colors-rofi-dark.rasi
│       ├── colors-rofi-light.rasi
│       ├── colors.scss
│       ├── colors.sh
│       ├── colors-speedcrunch.json
│       ├── colors-sway
│       ├── colors-tty.sh
│       ├── colors-wal-dmenu.h
│       ├── colors-wal-dwm.h
│       ├── colors-wal-st.h
│       ├── colors-wal-tabbed.h
│       ├── colors-wal.vim
│       ├── colors-waybar.css
│       ├── colors.Xresources
│       ├── colors.yml
│       ├── schemes
│       │   └── _mnt_ehdd_Pictures_wallpapers_8md6RX_png_dark_None_None_1.1.0.json
│       ├── sequences
│       └── wal
├── .config
│   ├── bspwm
│   │   └── bspwmrc -- bspwm config, must be executable
│   ├── cava
│   │   └── config
│   ├── discord-rpc
│   │   └── config.toml -- mpd-discord-rpc config
│   ├── dunst
│   │   └── dunstrc
│   ├── greenclip.cfg
│   ├── gtk-3.0
│   │   └── gtk.css
│   ├── herbstluftwm
│   │   └── autostart
│   ├── neofetch
│   │   └── config.conf
│   ├── openbox
│   │   ├── autostart -- basically xinitrc, but specific to openbox
│   │   ├── environment -- for setting environmental variables
│   │   └── rc.xml -- main openbox config
│   ├── qutebrowser
│   │   └── config.py
│   ├── rofi
│   │   └── config
│   ├── rtv
│   │   └── rtv.cfg
│   ├── sxhkd
│   │   └── sxhkdrc
│   ├── tint2 -- panel configs, -* dictates location
│   │   ├── tint2rc-bottom
│   │   ├── tint2rc-side
│   │   └── tint2rc-top
│   ├── vis
│   │   ├── colors
│   │   │   ├── index
│   │   │   ├── rainbow
│   │   │   └── rgb
│   │   └── config
│   ├── youtube-dl
│   │   └── config
│   └── youtube-viewer
│       └── youtube-viewer.conf
├── dwm
│   ├── config.def.h
│   └── patches
│       ├── dwm-gaplessgrid-20160731-56a31dc.diff
│       ├── dwm-gridmode-5.8.2.diff
│       ├── dwm-noborder-6.2.diff
│       ├── dwm-r1615-mpdcontrol.diff
│       ├── dwm-r1615-selfrestart.diff
│       ├── dwm-restartsig-20180523-6.2.diff
│       └── dwm-warp-git-20160626-7af4d43.diff
├── .emacs -- emacs config
├── escape_sequence_art -- art created out of escape sequences and unicode blocks, simply displaying the contents of the raw file in a terminal that properly supports escape sequences like xterm should render the art
│   ├── beserk
│   ├── dead
│   ├── haha
│   ├── loli
│   ├── mikasa
│   ├── mirror
│   └── trap
├── etc
│   ├── dnscrypt-proxy
│   │   └── dnscrypt-proxy.toml
│   └── portage
│       ├── make.conf
│       └── package.use
│           └── 00cpuflags
├── .gentoo -- gentoo-specific aliases, environmental variables, and/or functions to be sourced in .bashrc
├── .inputrc
├── kernel
│   ├── 4.19.72-gentoo-VALLEY-config
│   └── 5.4.31-ck-valley-config
├── .mint -- mint-specific aliases, environmental variables, and/or functions to be sourced in .bashrc
├── .mozilla
│   └── PROFILE
│       └── chrome -- making Firefox look better, mine removes a lot of UI elements and sets others to autohide
│           ├── userChrome.css
│           └── userContent.css
├── .mpd
│   ├── mpd.conf
│   └── mpd.conf (pi)
├── .mpdscribble
│   └── mpdscribble.conf
├── .nanorc
├── .ncmpcpp
│   ├── config
│   └── config-lyrics
├── README.md -- the thing that you are reading right now
├── scripts -- various scripts created or modified by me
│   ├── art.sh
│   ├── general
│   ├── ips.sh
│   └── temp
├── .tmux.conf
├── .vimrc
├── Windows
│   └── keybindings.ahk
├── .xinitrc -- for starting stuff before X
├── .xmonad
│   └── xmonad.hs
└── .Xresources -- for theming X apps


40 directories, 106 files
```
